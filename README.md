# Plantilla para Presentaciones del Departamento de Industrias - UTFSM
Esta es la plantilla para presentaciones según los formatos y exigencias del [Departamento de Industrias](http://www.industrias.usm.cl) de la Universidad Técnica Federico Santa María.

## Uso
Esta plantilla ocupa *beamer* como base.

Editar el archivo `beamerDI.tex`, y luego, en una consola (o su editor de latex favorito): 

	$ pdflatex beamerDI.tex
	$ bibtex bibliography
	$ pdflatex beamerDI.tex
	$ pdflatex beamerDI.tex

**NOTA:** `pdflatex` debe ejecutarse (en consola) tres veces, como se indica y en el orden mostrado para que latex pueda construir las Tablas de Contenidos y las referencias cruzadas de la Bibliografía.

### Version
1.0